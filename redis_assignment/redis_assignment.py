from pymongo import MongoClient
import redis


## connecting to mongo
client = MongoClient('localhost', 27017)

db = client['Demo']

coll = db.User

user_dict = {}

## getting information of users from mongo
users = coll.find({"phone_number": {"numberLong": "8197327887"}})

r = redis.Redis()

## setting user information to redis
for user in users:
    r.set(str(user['phone_number']['numberLong']), str(user))

## getting user information from redis
print(r.get('8197327887'))

## fetching all keys from redis
print(r.keys())

## deleting a record from redis
r.delete('8197327887')

## fetching all keys from redis
print(r.keys())
