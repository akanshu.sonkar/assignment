from distutils.log import debug
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client['Demo']

coll = db['Test']

app = Flask(__name__)


@app.route('/details/<phone_number>')
def details(phone_number):

    user_details = None

    users = coll.find({"phone_number": str(phone_number)})

    for user in users:
        user_details = user

    if user_details: return 'User Information :' + str(user_details)

    sample_record = {
    "phone_number" : str(phone_number),
    "updates" : {
    "answered_seq":0
    }
    }

    coll.insert_one(sample_record)

    users = coll.find({"phone_number": str(phone_number)})

    for user in users:
        user_details = user

    return "New User Added :" + str(user_details)


@app.route('/get_details',methods = ['POST', 'GET'])
def get_details():
    if request.method == 'POST':
        phone = request.form['ph']
        return redirect(url_for('details',phone_number = phone))

    else:
        return render_template('get_details.html')

if __name__ == '__main__':
   app.run(debug=True)