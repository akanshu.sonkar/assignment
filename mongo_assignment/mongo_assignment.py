from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client['Demo']

collection = db['User']

## Inserting sample record

# sample_record = {
# "last_triggered_date": {
# "$date": "2021-09-28T16:07:48.000Z"
# },
# "active_call": 0,
# "answered_seq": 0,
# "sequence_number": 0,
# "no_answer_seq": 0,
# "phone_number": {
# "$numberLong": "8197327887"
# },
# "first_name": "Avinash",
# }

# collection.insert_one(sample_record)

for doc in collection.find():
    print(doc)

collection.update_one({'first_name':'Avinash'}, {'$set': { "last_triggered_date": { "$date": "2021-09-30T16:07:48.000Z" }, "answered_seq": 1, "no_answer_seq": 1 } })

for doc in collection.find():
    print(doc)